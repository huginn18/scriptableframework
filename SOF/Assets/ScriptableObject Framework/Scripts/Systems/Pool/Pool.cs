﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SOF {
	public class Pool : MonoBehaviour {
		#region PublicFields		
		public GameObject prefab;
		public int startingAmount;
		public bool canExpand;
		public int expandAmount;
		#endregion
		#region PrivateFields
		[Space]
		[SerializeField]
		private List<GameObject> freeElements;
		[SerializeField]
		private List<GameObject> activeElements;

		private int currentID;
		#endregion

		#region UnityMethods
		private void Awake () {
			SpawnNewObjects (startingAmount);
		}
		private void Start () { }
		private void Update () { }
		#endregion

		#region PublicMethods
		public GameObject GetObjectFromPool () {
			if (freeElements.Count == 0) {
				if (canExpand)
					SpawnNewObjects (startingAmount);
				else {
					Debug.Log ("[" + this.gameObject.name + "] Pool is empty and can`t expand. Returning NULL");
					return null;
				}
			}

			GameObject go = freeElements[0];
			freeElements.Remove (go);
			activeElements.Add (go);

			PoolElement poolElement = go.GetComponent<PoolElement> ();
			if (poolElement == null) {
				poolElement = go.AddComponent<PoolElement> ();
			}
			poolElement.pool = this;
			go.SetActive (true);
			go.transform.SetParent (transform.root);
			go.transform.position = new Vector3 ();

			return go;
		}
		public void ReturnObjectToPool (PoolElement poolElement) {
			GameObject go = poolElement.gameObject;

			activeElements.Remove (go);
			freeElements.Add (go);
			go.transform.SetParent (this.transform);
			go.transform.position = new Vector3 ();
		}
		#endregion
		#region PrivateMethods
		private void SpawnNewObjects (int amount) {
			for (int i = 0; i < amount; i++) {
				GameObject go = Instantiate<GameObject> (prefab);

				go.SetActive (false);
				go.transform.SetParent (this.transform);
				go.transform.position = new Vector3 ();
				go.name = go.name + " " + currentID.ToString ();
				currentID++;

				freeElements.Add (go);
			}
		}
		#endregion
	}
}