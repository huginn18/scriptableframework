﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEngine;

namespace SOF {
	public class PoolElement : MonoBehaviour {
		#region PublicFields
		public Pool pool;
		#endregion
		#region PrivateFields
		#endregion

		#region UnityMethods
		private void OnDisable(){
			pool.ReturnObjectToPool(this);
		}
		#endregion

		#region PublicMethods
		public void ReturnToPool(){
			pool.ReturnObjectToPool(this);
		}
		#endregion
		#region PrivateMethods
		#endregion
	}
}