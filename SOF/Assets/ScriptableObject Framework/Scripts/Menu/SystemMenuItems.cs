﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEditor;
using System.Collections;

public class SystemMenuItems {
	#region PublicFields
	#endregion
	#region PrivateFields
	#endregion


	#region PublicMethods
	#endregion
	#region PrivateMethods
	[MenuItem("GameObject/SOF/AudioManager", false, 40)]
	private static void SpawnAudioManager(){
		GameObject go = new GameObject("AudioManager");
		SOF.AudioManager manager= go.AddComponent<SOF.AudioManager>();

		AudioSource music = go.AddComponent<AudioSource>();
		AudioSource sfx = go.AddComponent<AudioSource>();
		manager.music = music;
		manager.sfx = sfx;

		manager.music.clip = Resources.Load<AudioClip>("AmbientMusic");
	}

	[MenuItem("GameObject/SOF/Pool", false, 41)]
	private static void SpawnPool(){
		GameObject go = new GameObject("Pool");
		SOF.Pool pool = go.AddComponent<SOF.Pool>();
	}
	#endregion
}
