﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceTest : MonoBehaviour {
	[Header("Types")]
	public SOF.FloatReference floatReference;
	public SOF.IntReference intReference;
	public SOF.StringReference stringReference;

	public SOF.BoolReference boolReference;

	public SOF.CharReference charReference;

	public SOF.DoubleReference doubleReference;

	
	[Space]
	[Header("Vector")]
	public SOF.Vector2Reference vector2Reference;
	public SOF.Vector3Reference vector3Reference;

	[SOF.MinMaxFloatRange(-1,10)]
	public SOF.RangedFloat rangedFloat;
	[SOF.MinMaxIntRange(-5,20)]
	public SOF.RangedInt rangedInt;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
