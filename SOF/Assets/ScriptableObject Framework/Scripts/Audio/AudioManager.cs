﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEngine;

namespace SOF {
	public class AudioManager : MonoBehaviour {
		#region PublicFields
		public static AudioManager Instance;
		public AudioSource music;
		public AudioSource sfx;
		#endregion
		#region PrivateFields
		#endregion

		#region UnityMethods
		private void Awake () {
			if (Instance != null) {
				Destroy (this.gameObject);
				return;
			}
			Instance = this;

			DontDestroyOnLoad (this.gameObject);
		}
		#endregion

		#region PublicMethods
		public void PlaySFX (AudioClip clip) {
			sfx.clip = clip;
			sfx.Play();
		}
		public void PlayDynamicSFX(DynamicAudioClip dynamicClip){
			dynamicClip.Play(sfx);
		}
		#endregion
		#region PrivateMethods
		#endregion
	}
}