﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SOF {
	[CreateAssetMenu (fileName = "DynamicAudioClip", menuName = "SOF/Audio/DynamicAudioClip")]
	public class DynamicAudioClip : ScriptableObject {
		#region PublicFields
		public AudioClip[] clips;

		[Space]
		[MinMaxFloatRange (-3, 3)]
		public RangedFloat pitch;

		[MinMaxFloatRange (0, 1)]
		public RangedFloat volume;
		#endregion
		#region PrivateFields
		#endregion

		#region PublicMethods
		public void Play (AudioSource source) {
			source.clip = clips[Random.Range (0, clips.Length)];
			if (pitch.useConstant)
				source.pitch = pitch.constant;
			else
				source.pitch = Random.Range (pitch.minValue, pitch.maxValue);

			if (volume.useConstant)
				source.volume = volume.constant;
			else
				source.volume = Random.Range (volume.minValue, volume.maxValue);

			source.Play ();
		}
		#endregion
		#region PrivateMethods
		#endregion
	}
}