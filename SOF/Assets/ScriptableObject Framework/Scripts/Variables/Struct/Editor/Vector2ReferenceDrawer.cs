﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEditor;
using UnityEngine;

namespace SOF {
	[CustomPropertyDrawer(typeof(Vector2Reference))]
	public class Vector2ReferenceDrawer : PropertyDrawer {
		/// <summary>
		/// Options to display in the popup to select constant or variable.
		/// </summary>
		private readonly string[] popupOptions = { "Use Constant", "Use Variable" };

		/// <summary> Cached style to use to draw the popup button. </summary>
		private GUIStyle popupStyle;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
			if (popupStyle == null) {
				popupStyle = new GUIStyle (GUI.skin.GetStyle ("PaneOptions"));
				popupStyle.imagePosition = ImagePosition.ImageOnly;
			}

			label = EditorGUI.BeginProperty (position, label, property);
			position = EditorGUI.PrefixLabel (position, label);

			EditorGUI.BeginChangeCheck ();

			// Get properties
			SerializedProperty useConstant = property.FindPropertyRelative ("useConstant");
			SerializedProperty constantValue = property.FindPropertyRelative ("constantValue");
			SerializedProperty variable = property.FindPropertyRelative ("variable");

			// Calculate rect for configuration button
			Rect buttonRect = new Rect (position);
			buttonRect.yMin += popupStyle.margin.top;
			buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
			position.xMin = buttonRect.xMax;

			// Store old indent level and set it to 0, the PrefixLabel takes care of it
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			int result = EditorGUI.Popup (buttonRect, useConstant.boolValue ? 0 : 1, popupOptions, popupStyle);

			useConstant.boolValue = result == 0;

			EditorGUI.PropertyField (position,
				useConstant.boolValue ? constantValue : variable,
				GUIContent.none);

			if (EditorGUI.EndChangeCheck ())
				property.serializedObject.ApplyModifiedProperties ();

			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty ();
		}
	}
}


/*
															  `.//+ooo++/-    ``
								  ````......     `......--:+hddmNNMMMMMNNmdhy-.:sys+-.`
						  `.-:///+osydddmmmm.  +/+smNNNNNhhhhyyyyyyyyyhdmNMMMNhs+./yddho/-`
					`-/osyydmNMMMMMMMMMMMMMM.``MMm++yMds+shy:`   `.---..-:+osNMMMy/.:+yNMmhs/.
			  .--oydhhhddddddhhhdddddddmmNdo`-oMMMN::o/ymNMNh`  shdodmmmy/  `+oydNNmo:..ymNMNmd+-.
		   `.-/+oo+/--.``````````````.--//:`-smMMMMy/`dNMMMMM:` +hmsmMMMMN` .smds+shMdy/`-omNMMNmho:
		   ``....-::/+ossyyyhhhhhhhhhdddh-.:++//ydNMh/-/omMMMms. .:/oyhyo/  .hNMMds/+hNNy/`-odMMMMMmh-`
			   `.:+syyhdmNMMMMMMMMMMMMMMM:./mNmho//hmN:.`.sdMMNm/:. ````` `-oMMMMMNm--/dNmd-.-hNMMmdy-..
					  `.:/+osymNMMMMMMMMN--hMMMMMms:/hNh+  .+mMMMNds-`  -+ymMMMMMMh+/oo:sdMmy/`sdd:-.s/.
							``.--:oydmNNo-+MMMMMMMMo:.dNNh/.`-+dMMMMmho.+dMMMMMNd::/mNMm/+yMMNo:..-omy-
								  ```-+o`/yMMMMMMMMNh/`/yMNmy+:`smMMMMNs/-NMMMh+`+hNMMNd-:yMMNo..+mMM-`
										 `.:/ymNMMMMMMy/./hmMMmy.:+MMMMMs`/dNs-/yMMMMds-ydMMds /yMMMM
											 `./ymMMMMMNd/:/dNMMm+-smMMMh/ oy.sdMMMMh+/dMMMm+./dMMMNm
												 .omMMMMMMdo`ymMMdo`dMMMdo  .yNMMMdo`smMMMM--/MMMMdo.
												   -omMMMMMNh-oMMds`hMMMy: +hMMMMs:.dNMMMms`omMMNh-`
													`-mNMMMMM-+MMs::dMMNo`+dMMMmy`:oMMMMNs`ymMMms`
													  -yNMMNy.+Mm+.mNMM/::MMMMms .sMMMMMo/:MMMN/`
													   -oMMd`-oM-/sMMNd.+mMMMM/. ymMMMMN-/mMMN/`
														`dds yy++dMMNy`odMMMNs.`.NMMMMm/:sMMmy
														 -:::+++MMMMy/`NMMMMd` -oMMMMMh`hNMMs.
														  `-sshMMMMM:-hMMMMds  /MMMMMdo`mMMh/
														   `-dNMMMNy-/MMMMMo.  /MMMMM+:oNMM-`
														  `..-omMMm./yMMMMM/  .+MMMMM/-mMMN
														   .+s+.ymm hMMMMMd:  /yMMMMd::MMdo
															 /sd://.dMMMMMy-  sdMMMN+:/MMy.
															  :yNs:/dMMMMM+.  yNMMMN./sMMs
																dmNdNMMMMM/.  hMMMMN odMmo
																.yMMMMMMMM/.  dMMMMN yMMy/
																 :sMMMMMMM+.  dMMMMm yMM+.
																  .mNMMMMMy-  dMMMNd yMM:`
																   .dMMMMMd:  hNMMNh yMM:`
																	odMMMMN/  ymMMNd yMM:`
																	`/MMMMM+  +hMMNd yMM:`
																	 .sNMMMo- -oMMMm yMM:`
																	  `NMMMdo `+MMMN omM:`
																	   +hMMMd  /NMMN /yM+.
																	   `+NMMm-`-sMMN.-+Ms:
																		./MMNy.`.NMN/.:Mdo
																		 `dNMM:` mNNs--MNs
																		  `yNMs: +hMN:-dMy`
																		   ./NNs `oMM/`/md/
																			`/dh/ /dMo.`mNd
																			  :om..-Mdo ymM.`
																			   `-+-`hmh`:sMs-
																					.hmo`/NN+
																					 +hM--/Mh+
																					 `/My/ mNm`
																					  .oNh:-yNy-
																					   `dNm./sM:
																						-sMm+.y-
																						 :yMmy.`
																						  `mNMh-
																						   :hMN:
																							/hM:
																							 .m:
																							  :`
*/