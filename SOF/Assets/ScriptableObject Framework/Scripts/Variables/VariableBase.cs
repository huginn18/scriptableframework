﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEngine;

namespace SOF {
[CreateAssetMenu(fileName = "VariableBase", menuName = "SOF/VariableBase", order = -1)]
	public class VariableBase : ScriptableObject {
		#region PublicFields
		#if UNITY_EDITOR
		[Multiline]
		public string developerDescription;
		#endif
		#endregion
		#region PrivateFields
		#endregion

		#region PublicMethods
		#endregion
		#region PrivateMethods
		#endregion
	}
}