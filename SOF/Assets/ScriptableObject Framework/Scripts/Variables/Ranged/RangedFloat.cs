﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System;

namespace SOF {
[Serializable]
	public struct RangedFloat {
		#region PublicFields
		public float minValue;
		public float maxValue;

		public bool useConstant;
		public float constant;
		#endregion
		#region PrivateFields
		#endregion
	}
}