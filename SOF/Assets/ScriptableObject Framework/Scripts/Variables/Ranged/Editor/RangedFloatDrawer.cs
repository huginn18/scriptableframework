﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEditor;
using UnityEngine;

namespace SOF.EditorScirpts {
	[CustomPropertyDrawer (typeof (RangedFloat), true)]
	public class RangedFloatDrawer : PropertyDrawer {
		private GUIStyle popupStyle;
		private readonly string[] popupOptions = { "Use Constant", "Use Range" };

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
			label = EditorGUI.BeginProperty (position, label, property);
			position = EditorGUI.PrefixLabel (position, label);

			if (popupStyle == null) {
				popupStyle = new GUIStyle (GUI.skin.GetStyle ("PaneOptions"));
				popupStyle.imagePosition = ImagePosition.ImageOnly;
			}

			// Find vars
			SerializedProperty useConstant = property.FindPropertyRelative ("useConstant");
			SerializedProperty value = property.FindPropertyRelative ("constant");
			SerializedProperty minProp = property.FindPropertyRelative ("minValue");
			SerializedProperty maxProp = property.FindPropertyRelative ("maxValue");

			// Construct pop Up
			Rect buttonRect = new Rect (position);
			buttonRect.yMin += popupStyle.margin.top;
			buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
			position.xMin = buttonRect.xMax;

			// Store old indent level and set it to 0, the PrefixLabel takes care of it
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			int result = EditorGUI.Popup (buttonRect, useConstant.boolValue ? 0 : 1, popupOptions, popupStyle);

			useConstant.boolValue = result == 0;

			if (useConstant.boolValue) {
				EditorGUI.BeginChangeCheck();

				float v = value.floatValue;
				v = EditorGUI.FloatField (position, v);
				value.floatValue = v;
				
				// if(EditorGUI.EndChangeCheck())
				// 	property.serializedObject.ApplyModifiedProperties ();
			} else {

				float minValue = minProp.floatValue;
				float maxValue = maxProp.floatValue;

				float rangeMin = 0;
				float rangeMax = 1;

				var ranges = (MinMaxFloatRange[]) fieldInfo.GetCustomAttributes (typeof (MinMaxFloatRange), true);
				if (ranges.Length > 0) {
					rangeMin = ranges[0].Min;
					rangeMax = ranges[0].Max;
				}

				const float rangeBoundsLabelWidth = 40f;

				var rangeBoundsLabel1Rect = new Rect (position);
				rangeBoundsLabel1Rect.width = rangeBoundsLabelWidth;
				minValue = EditorGUI.FloatField (rangeBoundsLabel1Rect, minValue);
				if (minValue < rangeMin)
					minValue = rangeMin;
				minProp.floatValue = minValue;

				position.xMin += rangeBoundsLabelWidth;

				var rangeBoundsLabel2Rect = new Rect (position);
				rangeBoundsLabel2Rect.xMin = rangeBoundsLabel2Rect.xMax - rangeBoundsLabelWidth;
				maxValue = EditorGUI.FloatField (rangeBoundsLabel2Rect, maxValue);
				if (maxValue > rangeMax)
					maxValue = rangeMax;
				maxProp.floatValue = maxValue;

				position.xMin += 16;
				position.xMax -= rangeBoundsLabelWidth + 16;

				EditorGUI.BeginChangeCheck ();
				EditorGUI.MinMaxSlider (position, ref minValue, ref maxValue, rangeMin, rangeMax);
				if (EditorGUI.EndChangeCheck ()) {
					minProp.floatValue = minValue;
					maxProp.floatValue = maxValue;
				}
			}
			EditorGUI.EndProperty ();
		}
	}
}