﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System;

namespace SOF {
[Serializable]
	public struct RangedInt {
		#region PublicFields
		public int minValue;
		public int maxValue;

		public bool useConstant;
		public int constant;
		#endregion
		#region PrivateFields
		#endregion
	}
}