﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using UnityEngine;

namespace SOF {
[CreateAssetMenu(fileName = "CharVariable", menuName = "SOF/Type/CharVariable")]
	public class CharVariable :ScriptableObject{
		#region PublicFields
		#if UNITY_EDITOR
		[Multiline]
		public string developerDescription;
		#endif
		public char value{
			get{
				return _value;
			}
		}
		#endregion
		#region PrivateFields
		[SerializeField]
		private char _value;
		#endregion

		#region PublicMethods
		public void SetValue(char v){
			_value = v;
		}
		public void SetValue(CharReference v){
			_value = v.variable.value;
		}
		#endregion
		#region PrivateMethods
		#endregion
	}
}

/*
															  `.//+ooo++/-    ``
								  ````......     `......--:+hddmNNMMMMMNNmdhy-.:sys+-.`
						  `.-:///+osydddmmmm.  +/+smNNNNNhhhhyyyyyyyyyhdmNMMMNhs+./yddho/-`
					`-/osyydmNMMMMMMMMMMMMMM.``MMm++yMds+shy:`   `.---..-:+osNMMMy/.:+yNMmhs/.
			  .--oydhhhddddddhhhdddddddmmNdo`-oMMMN::o/ymNMNh`  shdodmmmy/  `+oydNNmo:..ymNMNmd+-.
		   `.-/+oo+/--.``````````````.--//:`-smMMMMy/`dNMMMMM:` +hmsmMMMMN` .smds+shMdy/`-omNMMNmho:
		   ``....-::/+ossyyyhhhhhhhhhdddh-.:++//ydNMh/-/omMMMms. .:/oyhyo/  .hNMMds/+hNNy/`-odMMMMMmh-`
			   `.:+syyhdmNMMMMMMMMMMMMMMM:./mNmho//hmN:.`.sdMMNm/:. ````` `-oMMMMMNm--/dNmd-.-hNMMmdy-..
					  `.:/+osymNMMMMMMMMN--hMMMMMms:/hNh+  .+mMMMNds-`  -+ymMMMMMMh+/oo:sdMmy/`sdd:-.s/.
							``.--:oydmNNo-+MMMMMMMMo:.dNNh/.`-+dMMMMmho.+dMMMMMNd::/mNMm/+yMMNo:..-omy-
								  ```-+o`/yMMMMMMMMNh/`/yMNmy+:`smMMMMNs/-NMMMh+`+hNMMNd-:yMMNo..+mMM-`
										 `.:/ymNMMMMMMy/./hmMMmy.:+MMMMMs`/dNs-/yMMMMds-ydMMds /yMMMM
											 `./ymMMMMMNd/:/dNMMm+-smMMMh/ oy.sdMMMMh+/dMMMm+./dMMMNm
												 .omMMMMMMdo`ymMMdo`dMMMdo  .yNMMMdo`smMMMM--/MMMMdo.
												   -omMMMMMNh-oMMds`hMMMy: +hMMMMs:.dNMMMms`omMMNh-`
													`-mNMMMMM-+MMs::dMMNo`+dMMMmy`:oMMMMNs`ymMMms`
													  -yNMMNy.+Mm+.mNMM/::MMMMms .sMMMMMo/:MMMN/`
													   -oMMd`-oM-/sMMNd.+mMMMM/. ymMMMMN-/mMMN/`
														`dds yy++dMMNy`odMMMNs.`.NMMMMm/:sMMmy
														 -:::+++MMMMy/`NMMMMd` -oMMMMMh`hNMMs.
														  `-sshMMMMM:-hMMMMds  /MMMMMdo`mMMh/
														   `-dNMMMNy-/MMMMMo.  /MMMMM+:oNMM-`
														  `..-omMMm./yMMMMM/  .+MMMMM/-mMMN
														   .+s+.ymm hMMMMMd:  /yMMMMd::MMdo
															 /sd://.dMMMMMy-  sdMMMN+:/MMy.
															  :yNs:/dMMMMM+.  yNMMMN./sMMs
																dmNdNMMMMM/.  hMMMMN odMmo
																.yMMMMMMMM/.  dMMMMN yMMy/
																 :sMMMMMMM+.  dMMMMm yMM+.
																  .mNMMMMMy-  dMMMNd yMM:`
																   .dMMMMMd:  hNMMNh yMM:`
																	odMMMMN/  ymMMNd yMM:`
																	`/MMMMM+  +hMMNd yMM:`
																	 .sNMMMo- -oMMMm yMM:`
																	  `NMMMdo `+MMMN omM:`
																	   +hMMMd  /NMMN /yM+.
																	   `+NMMm-`-sMMN.-+Ms:
																		./MMNy.`.NMN/.:Mdo
																		 `dNMM:` mNNs--MNs
																		  `yNMs: +hMN:-dMy`
																		   ./NNs `oMM/`/md/
																			`/dh/ /dMo.`mNd
																			  :om..-Mdo ymM.`
																			   `-+-`hmh`:sMs-
																					.hmo`/NN+
																					 +hM--/Mh+
																					 `/My/ mNm`
																					  .oNh:-yNy-
																					   `dNm./sM:
																						-sMm+.y-
																						 :yMmy.`
																						  `mNMh-
																						   :hMN:
																							/hM:
																							 .m:
																							  :`
*/