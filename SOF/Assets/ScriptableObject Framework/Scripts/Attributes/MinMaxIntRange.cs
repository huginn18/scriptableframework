﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System;
using System.Collections;
using UnityEngine;

namespace SOF {
	public class MinMaxIntRange : Attribute {
		#region PublicFields
		public int Min { get; protected set; }
		public int Max { get; protected set; }
		#endregion
		#region PrivateFields
		#endregion

		public MinMaxIntRange (int min, int max) {
			Min = min;
			Max = max;
		}

		#region PublicMethods
		#endregion
		#region PrivateMethods
		#endregion
	}
}