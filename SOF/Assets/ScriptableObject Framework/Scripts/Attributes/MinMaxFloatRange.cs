﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System;
using System.Collections;
using UnityEngine;

namespace SOF {
	public class MinMaxFloatRange : Attribute {
		#region PublicFields
		public float Min { get; protected set; }
		public float Max { get; protected set; }
		#endregion
		#region PrivateFields
		#endregion

		public MinMaxFloatRange (float min, float max) {
			Min = min;
			Max = max;
		}

		#region PublicMethods
		#endregion
		#region PrivateMethods
		#endregion
	}
}